package com.lsw.clothesmanager.repository;

import com.lsw.clothesmanager.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClothesRepository extends JpaRepository<Clothes, Long> {
}
