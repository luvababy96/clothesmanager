package com.lsw.clothesmanager.controller;

import com.lsw.clothesmanager.model.ClothesRequest;
import com.lsw.clothesmanager.service.ClothesService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clothes")
public class ClothesController {

    private final ClothesService clothesService;

    @PostMapping("/data")
    public String setClothes(@RequestBody ClothesRequest request) {
        clothesService.setClothes(request.getColor(),request.getType(), request.getThickness(), request.getWashing());
        return "OK";
    }

}
