package com.lsw.clothesmanager.service;

import com.lsw.clothesmanager.entity.Clothes;
import com.lsw.clothesmanager.repository.ClothesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClothesService {

    private final ClothesRepository clothesRepository;

    public void setClothes(String color, String type, String thickness, String washing) {
        Clothes addData = new Clothes();
        addData.setColor(color);
        addData.setType(type);
        addData.setThickness(thickness);
        addData.setWashing(washing);

        clothesRepository.save(addData);

    }
}
