package com.lsw.clothesmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClothesRequest {

    private String color;

    private String type;

    private String thickness;

    private String washing;

}
